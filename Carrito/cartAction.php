<?php
// initialize shopping cart class
include './Cart.php';
$cart = new Cart;

// include database configuration file
include './dbConfig.php';

if(isset($_REQUEST['action']) && !empty($_REQUEST['action'])){
    if($_REQUEST['action'] == 'addToCart' && !empty($_REQUEST['id'])){
        $productID = $_REQUEST['id'];
        if($_REQUEST['x']==2){
        // get product details
        $query = $db->query("SELECT * FROM Plantas WHERE Id = ".$productID);
        $row = $query->fetch_assoc();
        $itemData = array(
            'id' => $row['Id'],
            'name' => $row['Nombre'],
            'price' => $row['Precio'],
            'qty' => 1,
            'tab' => $_REQUEST['x'],
            'cant' =>$row['Cantidad']
        );
        }
        else{
        // get product details
        $query = $db->query("SELECT * FROM Productos WHERE Id = ".$productID);
        $row = $query->fetch_assoc();
        $itemData = array(
            'id' => $row['Id'],
            'name' => $row['Nombre'],
            'price' => $row['Precio'],
            'qty' => 1,
            'tab' => $_REQUEST['x'],
            'cant' =>$row['Cantidad']
        );
        }
        
        $insertItem = $cart->insert($itemData);
        //$redirectLoc = $insertItem?'viewCart.php':'../plantas.php';
        $redirectLoc = $insertItem?'viewCart.php':'./viewCart.php';
        header("Location: ".$redirectLoc);
    }elseif($_REQUEST['action'] == 'updateCartItem' && !empty($_REQUEST['id'])){
        $itemData = array(
            'rowid' => $_REQUEST['id'],
            'qty' => $_REQUEST['qty']
        );
        $updateItem = $cart->update($itemData);
        echo $updateItem?'ok':'err';die;
    }elseif($_REQUEST['action'] == 'removeCartItem' && !empty($_REQUEST['id'])){
        $deleteItem = $cart->remove($_REQUEST['id']);
        header("Location: ./viewCart.php");

    }elseif($_REQUEST['action'] == 'placeOrder' && $cart->total_items() > 0 && !empty($_SESSION['ID_ses'])){
        // insert order details into database
        $insertOrder = $db->query("INSERT INTO Ventas (Usuario_Id, Total) VALUES ('".$_SESSION['ID_ses']."', '".$cart->total()."')");
        
        if($insertOrder){
            $orderID = $db->insert_id;
            $sql = '';
            // get cart items
            $cartItems = $cart->contents();
            foreach($cartItems as $item){
                if($item['tab']==2){
                    $xd= $item['id'];
                    $sql .= "INSERT INTO items_Plant(Ventas_Id, Plantas_Id, Cantidad) VALUES ('".$orderID."', '".$xd."', '".$item['qty']."');";
                    
                    //$sql .= "UPDATE Plantas SET Cantidad="$item['qty']" WHERE Id='$xd' ;";
                }
                else{
                    $sql .= "INSERT INTO items_Product(Ventas_Id, Productos_Id, Cantidad) VALUES ('".$orderID."', '".$item['id']."', '".$item['qty']."');";
                }
            }
            // insert order items into database
            $insertOrderItems = $db->multi_query($sql);
            
            if($insertOrderItems){
                $cart->destroy();
                header("Location: orderSuccess.php?id=$orderID");
            }else{
                header("Location: ./checkout.php");
            }
        }else{
            header("Location: ./checkout.php");
        }
    }else{
        header("Location: ../index.php");
    }
}else{
    header("Location: ../index.php");
}