<?php

// include database configuration file
include 'dbConfig.php';

// initializ shopping cart class
include 'Cart.php';
$cart = new Cart;

// redirect to home if cart is empty
if($cart->total_items() <= 0){
    header("Location: index.php");
}

// set customer ID in session
$_SESSION['sessCustomerID'] = 1;

// get customer details by session customer ID
//$query = $db->query("SELECT * FROM Usuarios WHERE Id = ".$_SESSION['sessCustomerID']);
$query = $db->query("SELECT * FROM Info_Usua WHERE Id = " . $_SESSION['ID_ses']);
$custRow = $query->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="icon" href="../img/icons/logo.ico">
    <title>Pago</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>
    .table{width: 65%;float: left;}
    .shipAddr{width: 30%;float: left;margin-left: 30px;}
    .footBtn{width: 95%;float: left;}
    .orderBtn {float: right;}
    body{
    background: -webkit-linear-gradient(left, #3931af, #00c6ff);
    }
    .emp-profile{
    padding: 3%;
    margin-top: 3%;
    margin-bottom: 3%;
    border-radius: 0.5rem;
    background: #fff;
    }
    </style>
</head>
<body>
<div class="container emp-profile">
    <h1>Vista previa de su compra</h1>
    <table class="table">
    <thead>
        <tr>
            <th>Articulos</th>
            <th>Precio</th>
            <th>Cantidad</th>
            <th>Subtotal</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if($cart->total_items() > 0){
            //get cart items from session
            $cartItems = $cart->contents();
            foreach($cartItems as $item){
        ?>
        <tr>
            <td><?php echo $item["name"]; ?></td>
            <td><?php echo '$'.$item["price"].' Pesos'; ?></td>
            <td><?php echo $item["qty"]; ?></td>
            <td><?php echo '$'.$item["subtotal"].' Pesos'; ?></td>
        </tr>
        <?php } }else{ ?>
        <tr><td colspan="4"><p>No hay articulos en su carrito......</p></td>
        <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"></td>
            <?php if($cart->total_items() > 0){ ?>
            <td class="text-center"><strong>Total <?php echo '$'.$cart->total().' Pesos'; ?></strong></td>
            <?php } ?>
        </tr>
    </tfoot>
    </table>
    <div class="shipAddr">
        <h2>Detalles de Compras</h2>
        <h4>Nombre:</h4>
        <p><?php echo $custRow['Nombre']; ?> <?php echo $custRow['ApPa']; ?> <?php echo $custRow['ApMa']; ?></p>
        
        <h4>Correo Electronico:</h4>
        <p><?php echo $custRow['Correo']; ?></p>
        <h4>Direccion:</h4>
        <!--Verificar si tiene Direccion-->
        <?php if($custRow['Id_Dir']==0){?>
        <p>Agrege una direccion para poder continuar</p>
        </div>
        <div class="footBtn">
            <a href="javascript:history.back(-1);" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Regresar</a>
            <a href="../Profile/profile.php" class="btn btn-success orderBtn">Perfil<i class="glyphicon glyphicon-menu-right"></i></a>
        </div>
        <?php } else{?>
            <p>Fraccionamiento/Colonia: <?php echo $custRow['Fracc']; ?></p>
            <p>Calle:<?php echo $custRow['Calle']; ?></p>
            <p>SMza: <?php echo $custRow['SMza']; ?> Mza: <?php echo $custRow['Mza']; ?> CP: <?php echo $custRow['CP']; ?></p>
            <p>Ciudad: <?php echo $custRow['Ciudad']; ?></p>
        </div>
        <div class="footBtn">
            <a href="javascript:history.back(-1);" class="btn btn-warning"><i class="glyphicon glyphicon-menu-left"></i> Regresar</a>
            <a href="cartAction.php?action=placeOrder" class="btn btn-success orderBtn">Proceder a Pagar <i class="glyphicon glyphicon-menu-right"></i></a>
        </div>
        <div>
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="3L7NQVWMPK7QS">
<input type="image" src="https://www.paypalobjects.com/es_XC/MX/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal, la forma más segura y rápida de pagar en línea.">
<img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
</form>
        </div>
        <?php } ?>
        <!---->
</div>
</body>
</html>

