<?php
require_once './libs/DataBase.php';
$pdo = DataBase::getInstance();

$stmt = $pdo->prepare("SELECT * FROM product_info"); 
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_ASSOC);

    /* creamos un while para optener fila a fila los refultados devueltos mediante fetch() 
    while ($row = $stmt->fetch()) {
        echo $row['Nombre'] . " - " . $row['Imagen'] ."<br>";
        echo "xdxdxd";
    }*/
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--Materialize css-->
    <link rel="stylesheet" href="./css/sty.css">
  <!--Otro CSS
  <link rel="stylesheet" href="./css/Supreme.css">
  -->
  <link rel="stylesheet" href="./css/chater.css">
  
  <link rel="icon" href="./img/icons/logo.ico">
    <title>Producto</title>
  </head>
  <body>
  <!--Barra de navegacion-->
  <?php include('Nav.php'); ?>    
  <!--Fin Barra-->
    
    <!--Carrusel de fotos-->
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active parallax" id="product1">
            </div>
            <div class="carousel-item parallax" id="product2">
            </div>
            <div class="carousel-item parallax" id="product3">
            </div>
        </div>
    </div>
    <!--Fin carrusel de fotos-->
    <!--Cartas Productos-->
  <div class="container marketing">
        <div id="cart" class="div-cent row">
          <!--Se manda a llamar mediante un while todas las plantas de la BD -->
          <?php while ($row = $stmt->fetch()) {
            if($row['Cantidad']!=0){?>
            <div class="card" style="width: 18rem;">
              <img src="img/productos/<?php Echo $row['Imagen']?>"  height="175" class="card-img-top" alt="...">
              <div class="card-body">
              <h3 class="card-title"><?php echo $row['Nombre'] ?></h3>
              <p class="card-text">$<?php echo $row['Precio'] ?></p>              
              <a class="btn btn-success" href="./Carrito/cartAction.php?action=addToCart&id=<?php echo $row["Id"]; ?>&x=1">Add to cart</a>

      </div>
    </div>
    <?php }} ?>
        </div>
    </div>    

  <!--Fin Cartas Productos-->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>