<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
  <!--Otro CSS
  <link rel="stylesheet" href="./css/Supreme.css">
  -->
  <link rel="stylesheet" href="./css/chater.css">
  <style type="text/css">
.emp-profile{
    padding: 3%;
    margin-top: 3%;
    margin-bottom: 3%;
    border-radius: 0.5rem;
    background: #fff;
}
  </style>
  
  <link rel="icon" href="./img/icons/logo.ico">
    <title>Acerca de...</title>
</head>

<body>
  <!--Barra de navegacion-->
  <?php include('Nav.php'); ?>    
  <!--Fin Barra-->
    <!--Carrusel de fotos-->
    <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active parallax" id="info1">
            </div>
            <div class="carousel-item parallax" id="info2">
            </div>
            <div class="carousel-item parallax" id="info3">
            </div>
        </div>
    </div>
    <!--Fin carrusel de fotos-->
    <section class="section section-lg bg-gray-1">
        <div class="container emp-profile">
            <div class="row row-50">
                <div style="border-radius: 20px;" class="col-lg-6 pr-xl-5"><img src="img/Banners/basil-2053350_1920.jpg" alt="" width="518"
                        height="434" />
                </div>
                <div class="col-lg-6">
                    <h2>Sobre nosotros...</h2>
                    <div class="text-with-divider">
                        <div class="divider"></div>
                        <h4></h4>
                    </div>
                    <p>Hoy en día prácticamente todo se cultiva en contenedor (maceta de cultivo de plástico) pero
                        cuando
                        Chatterify comenzó, era ir en contra de las técnicas tradicionales utilizadas hasta el momento.
                        También a nivel estatal y universitario había dudas acerca de éxito que podía tener una planta
                        al ser
                        trasplantada de un cultivo en contenedor a la tierra.
                        Hoy prácticamente todos los cultivos se producen en contenedor tanto para jardinería como para
                        reforestación, reposición de marras en fruticultura, etc.
                        El cultivo en contenedor permite trasplantar en cualquier época de año con total éxito de
                        arraigue sin que
                        la planta sufra.</p>
                </div>
            </div>
        </div>
    </section>

    <!--Info empresa--
    <h1>Nuestra empresa</h1>
    <div id="cart" class="div-cent">
        <img src="img/plantas/9556710965_efe7de9eb8_b.jpg" width="360" height="360">
        <div id="x">
            Hoy en día prácticamente todo se cultiva en contenedor (maceta de cultivo de plástico) pero cuando
            Chatterify comenzó, era ir en contra de las técnicas tradicionales utilizadas hasta el momento.
            También a nivel estatal y universitario había dudas acerca de éxito que podía tener una planta al ser
            trasplantada de un cultivo en contenedor a la tierra.
            Hoy prácticamente todos los cultivos se producen en contenedor tanto para jardinería como para
            reforestación, reposición de marras en fruticultura, etc.
            El cultivo en contenedor permite trasplantar en cualquier época de año con total éxito de arraigue sin que
            la planta sufra.
        </div>
    </div>-->
    <!--Fin info-->
    <!-- FOOTER -->
    <object type="text/x-scriptlet" width=100% height="270" scrolling="no" data="Footer.html">
    </object>
    <!--Fin Footer-->
    <!--Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>