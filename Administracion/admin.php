<?php
require_once '../libs/DataBase.php';
$pdo = DataBase::getInstance();

$stmt = $pdo->query("SELECT * FROM Plantas")->fetchAll();

$produ = $pdo->query("SELECT * FROM Productos")->fetchAll(); 

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../img/icons/logo.ico">

  <title>Administracion</title>

  <!-- Bootstrap core CSS -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


  <!-- Custom styles for this template -->
  <link href="../css/dashboard.css" rel="stylesheet">
</head>

<body>
  
  <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item ">
          <a class="nav-link" href="admin.php">Dashboard <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="cuentas.php">Cuentas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="Papelera.php">Papelera</a>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0" action="../libs/logout.php">
        <button class="btn btn-outline-light my-2 my-sm-0"  type="submit">Cerrar Sesion</button>
      </form>
    </div>
  </nav>
  <!--Columna-NAV-->
  <div class="container-fluid">
    <div class="row">
      <nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="#Inicio">Inicio <span class="sr-only">(current)</span></a>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              Plantas
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#TaPla">Tabla</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#AgrePla">Agregar</a>
              <a class="dropdown-item" href="#EditPla">Editar</a>
            </div>
          </li>

          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              Productos
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#TaPro">Tabla</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#AgrePro">Agregar</a>
              <a class="dropdown-item" href="#EditPro">Editar</a>
            </div>
          </li>
      </nav>
      <!--Fin Columna-NAV-->
      <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3" id='Inicio'>
        <h1>Dashboard</h1>

        <!--Tabla de Productos/Plantas-->
        <h2 id='TaPla'>Plantas</h2>
        <div class="table-responsive">
          <table class="table table-striped">
            <tbody>
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Tipo</th>
                  <th>Cantidad</th>
                  <th>Precio</th>
                  <th>Descripcion</th>
                  <th>Cantidad</th>
                  <th>Imagen</th>
                  <th>Eliminar</th>
                </tr>
              </thead>
              <?php foreach ($stmt as $row) {?>
              <tr>
                <td><?php Echo $row['Nombre']?></td>
                <td><?php Echo $row['Tipo']?></td>
                <td><?php Echo $row['Cantidad']?></td>
                <td>$<?php Echo $row['Precio']?></td>
                <td><?php Echo $row['Descripcion']?></td>
                <td><?php Echo $row['Cantidad']?></td>
                <td><img src="../img/plantas/<?php Echo $row['Imagen']?>" height="150" width="175" class="card-img-top"
                    alt="..."></td>
                <td>
                <form action="../libs/delete.php?de=1" method="POST" enctype="multipart/form-data">
                  <textarea style="display:none;" name='Id'><?php echo $row['Id']; ?></textarea>
                  <button type="submit" class="btn btn-outline-danger btn-lg ">Eliminar</button>
                </form>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!--Formulario para agregar una planta-->
        <div id='AgrePla'>
          <h1>Agregar Nueva Planta</h1>
          <div class="container">
            <form action="../libs/upload.php?de=1" method="POST" enctype="multipart/form-data">
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label>Nombre de la planta</label>
                  <input type="text" class="form-control" placeholder="Nombre" name="Nombre" required>
                </div>
                <div class="form-group col-md-4">
                  <label>Tipo de Planta</label>
                  <select class="form-control" name="Tipo">
                    <option value="Arbol">Árbol</option>
                    <option value="Arbustos">Arbustos</option>
                    <option value="Trepadoras">Trepadoras</option>
                    <option value="Cactus">Cactus</option>
                    <option value="Crasas">Crasas</option>
                    <option value="Herbaceas">Herbáceas</option>
                    <option value="Hortícolas">Hortícolas</option>
                    <option value="Palmeras">Palmeras</option>
                  </select>
                  </select>
                </div>
                <div class="form-group col-md-2">
                  <label>Cantidad</label>
                  <input type="number" class="form-control" placeholder="0-?" name="Cantidad"  min="1" pattern="^[0-9]+" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Precio</label>
                  <input type="number" class="form-control" placeholder="$???" name="Precio" min="1" pattern="^[0-9]+" required>
                </div>
              </div>
              <div class="form-group">
                <label>Breve Descripcion</label>
                <textarea class="form-control" placeholder="Esta planta..." name="Descrip"></textarea>
              </div>
              <div class="form-group">
                <label>Agregar imagen</label>
                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="imagen" required>
              </div>
              <div id="preview" height="150" width="175">
                
              </div>
              <button type="submit" value="sumit" class="btn btn-outline-info btn-lg btn-block"
                name="entrar">Guardar</button>
            </form>
          </div>
        </div>
        <!--fin formulario-->
        <!--Formulario para Editar una planta-->
        <div>
          <h1 id='EditPla'>Editar Planta</h1>

          <div class="container">
            <form action="./admin.php#EditPla" method="POST" enctype="multipart/form-data">
              <div class="form-row">
                <label>Elija la planta a editar:</label>
                <select class="form-control" name="Id" onchange="this.form.submit()" placeholder="">
                  <option value="null"></option>
                  <?php foreach ($stmt as $row) { ?>
                  <option value="<?php echo $row['Id']?>"><?php echo $row['Nombre'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </form>

            <form action="../libs/update.php?de=1" method="POST" enctype="multipart/form-data">
              <?php 
        if(isset ($_POST['Id'])){
          $Id = $_POST['Id'];
        }
        else {
          $Id = 1;
        }
        $stmt2 = $pdo->query("SELECT * FROM Plantas where Id='$Id' "); 
        $row1 = $stmt2->fetch();
        ?>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label>Nombre de la planta</label>
                  <input type="text" class="form-control" placeholder="<?php echo $row1['Nombre'];?>" name="Nombre" required>
                  <textarea style="display:none;" name='Id'><?php echo $Id; ?></textarea>
                </div>
                <div class="form-group col-md-4">
                  <label>Tipo de Planta</label>
                  <select class="form-control" name="Tipo">
                    <option value="Arbol">Árbol</option>
                    <option value="Arbustos">Arbustos</option>
                    <option value="Trepadoras">Trepadoras</option>
                    <option value="Cactus">Cactus</option>
                    <option value="Crasas">Crasas</option>
                    <option value="Herbaceas">Herbáceas</option>
                    <option value="Hortícolas">Hortícolas</option>
                    <option value="Palmeras">Palmeras</option>
                  </select>
                </div>
                <div class="form-group col-md-2">
                  <label>Cantidad</label>
                  <input type="number" class="form-control" placeholder="<?php echo $row1['Cantidad'];?>" name="Cantidad" min="1" pattern="^[0-9]+" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Precio</label>
                  <input type="number" class="form-control" placeholder="$<?php echo $row1['Precio'];?>" name="Precio" min="1" pattern="^[0-9]+" required>
                </div>
              </div>

              <div class="form-group">
                <label>Breve Descripcion</label>
                <textarea class="form-control" name="Descrip"><?php echo $row1['Descripcion'];?></textarea>
              </div>
              <div></div>
              <button type="submit" value="sumit" class="btn btn-outline-info btn-lg btn-block" name="entrar">Guardar Cambios</button>
            </form>
          </div>

        </div>
        <!--fin formulario-->
        



        <!--Tabla de Productos/Plantas-->
        <h2 id='TaPro'>Productos</h2>
        <div class="table-responsive">
          <table class="table table-striped">
            <tbody>
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Total Vendidos</th>
                  <th>Precio</th>
                  <th>Imagen</th>
                  <th>Eliminar</th>
                </tr>
              </thead>
              <?php foreach ($produ as $row){?>
              <tr>
                <td><?php Echo $row['Nombre']?></td>
                <td><?php Echo $row['Cantidad']?></td>
                <td><?php Echo $row['Total_Vendidos']?></td>
                <td>$<?php Echo $row['Precio']?></td>
                <td><img src="../img/Productos/<?php Echo $row['Imagen']?>" height="100" width="100" alt="..."></td>
                <td>
                <form action="../libs/delete.php?de=2" method="POST" enctype="multipart/form-data">
                  <textarea style="display:none;" name='Id'><?php echo $row['Id']; ?></textarea>
                  <button type="submit" class="btn btn-outline-danger btn-lg ">Eliminar</button>
                </form>
              </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!--fin tabla-->
        <!--Formulario para agregar un prodcuto -->

        <div id='AgrePro'>
          <h1>Agregar Nuevo Producto</h1>
          <div class="container">
            <form action="../libs/upload.php?de=2" method="POST" enctype="multipart/form-data">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Nombre del producto</label>
                  <input type="text" class="form-control" placeholder="Nombre" name="Nombre" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Cantidad</label>
                  <input type="number" class="form-control" placeholder="0-?" name="Cantidad" min="1" pattern="^[0-9]+" required>
                </div>

                <div class="form-group col-md-2">
                  <label>Precio</label>
                  <input type="number" class="form-control" placeholder="$???" name="Precio" min="1" pattern="^[0-9]+" required>
                </div>
              </div>
          </div>
          <div class="form-group">
            <label>Agregar imagen</label>
            <input type="file" class="form-control-file" id="exampleFormControlFile2" name="imagen">
          </div>
          <div id="preview2"></div>
          <button type="submit" value="sumit" class="btn btn-outline-info btn-lg btn-block"
            name="entrar">Guardar</button>
          </form>
        </div>
        <!--fin formulario-->
        <!--Formulario para Editar una planta-->
        <div>
          <h1 id='EditPro'>Editar Producto</h1>

          <div class="container">
            <form action="./admin.php#EditPro" method="POST" enctype="multipart/form-data">
              <div class="form-row">
                <label>Elija el producto a editar:</label>
                <select class="form-control" name="Id" onchange="this.form.submit()" placeholder="">
                    <option value="null"></option>
                  <?php foreach ($produ as $row) { ?>
                  <option value="<?php echo $row['Id']?>"><?php echo $row['Nombre'] ?></option>
                  <?php } ?>
                </select>
              </div>
            </form>

            <form action="../libs/update.php?de=2" method="POST" enctype="multipart/form-data">
              <?php 
        if(isset ($_POST['Id'])){
          $Id = $_POST['Id'];
        }
        else {
          $Id = 1;
        }
        $stmt2 = $pdo->query("SELECT * FROM Productos where Id='$Id' "); 
        $row1 = $stmt2->fetch();
        ?>

              <div class="form-row">
                <div class="form-group col-md-6">
                  <label>Nombre de la planta</label>
                  <input type="text" class="form-control" placeholder="<?php echo $row1['Nombre'];?>" name="Nombre" required>
                  <textarea style="display:none;" name='Id'><?php echo $Id; ?></textarea>
                </div>
                <div class="form-group col-md-2">
                  <label>Cantidad</label>
                  <input type="number" class="form-control" placeholder="<?php echo $row1['Cantidad'];?>"
                    name="Cantidad" min="1" pattern="^[0-9]+" required>
                </div>
                <div class="form-group col-md-2">
                  <label>Precio</label>
                  <input type="number" class="form-control" placeholder="$<?php echo $row1['Precio'];?>" name="Precio"
                    min="1" pattern="^[0-9]+" required>
                </div>
              </div>
              <div></div>
              <button type="submit" value="sumit" class="btn btn-outline-info btn-lg btn-block" name="entrar">Guardar
                Cambios</button>
            </form>
          </div>

        </div>
        <!--fin formulario-->

    </div>
    </main>
  </div>
  </div>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
    integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
    integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
    crossorigin="anonymous"></script>
  <script src="../JavaScrip/bootstrap.min.js"></script>
  <!--Activar - desactivar class active de un nav-item -->
  <script>
    $('li a').click(function (e) {
      //e.preventDefault();
      var $this = $(this);
      $this.closest('ul').find('li.active,a.active').removeClass('active');
      $this.addClass('active');
      $this.parent().addClass('active');

    });</script>
  <!--previsualizar una imagen-->
  <script>
    document.getElementById("exampleFormControlFile1").onchange = function (e) {
      // Creamos el objeto de la clase FileReader
      let reader = new FileReader();

      // Leemos el archivo subido y se lo pasamos a nuestro fileReader
      reader.readAsDataURL(e.target.files[0]);

      // Le decimos que cuando este listo ejecute el código interno
      reader.onload = function () {
        let preview = document.getElementById('preview'),
          image = document.createElement('img');

        image.src = reader.result;

        preview.innerHTML = '';
        preview.append(image);
      };
    }
    document.getElementById("exampleFormControlFile2").onchange = function (e) {
      // Creamos el objeto de la clase FileReader
      let reader = new FileReader();

      // Leemos el archivo subido y se lo pasamos a nuestro fileReader
      reader.readAsDataURL(e.target.files[0]);

      // Le decimos que cuando este listo ejecute el código interno
      reader.onload = function () {
        let preview = document.getElementById('preview2'),
          image = document.createElement('img');

        image.src = reader.result;

        preview.innerHTML = '';
        preview.append(image);
      };
    }
  </script>
</body>

</html>