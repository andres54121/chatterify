<?php
require_once '../libs/DataBase.php';
$pdo = DataBase::getInstance();

$stmt = $pdo->query("SELECT * FROM Admins")->fetchAll();

$client = $pdo->query("SELECT * FROM Info_Usua")->fetchAll(); 
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../img/icons/logo.ico">

  <title>Administracion</title>

  <!-- Bootstrap core CSS -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


  <!-- Custom styles for this template -->
  <link href="../css/admin.css" rel="stylesheet">
</head>

<body>

  <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item ">
          <a class="nav-link" href="admin.php">Dashboard <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="cuentas.php">Cuentas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="Papelera.php">Papelera</a>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0" action="../libs/logout.php">
        <button class="btn btn-outline-light my-2 my-sm-0"  type="submit">Cerrar Sesion</button>
      </form>
    </div>
  </nav>
  <!--Columna-NAV-->
  <div class="container-fluid">
    <div class="row">
      <nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="#Inicio">Inicio <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#TaPla">Administradores <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#TaPro">Clientes <span class="sr-only">(current)</span></a>
          </li>
      </nav>
    </div>
  </div>
  <!--Fin Columna-NAV-->
  <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3" id='Inicio'>
    <!--Tabla de Admins-->
    <h1 id='TaPla'>Administradores</h1>
    <div class="inner cover"><br>
    <!--Boton Editar Perfil-->
    <button class="btn btn-outline-primary btn-lg btn-block"
      onclick="document.getElementById('modal-wrapper').style.display='block'">Agregar Nuevo Administrador</button>
    <div id="modal-wrapper" class="modal">
      <form class="modal-content animate" action="../libs/create-account.php?lvl=1" method="POST">
        <div class="imgcontainer">
          <span onclick="document.getElementById('modal-wrapper').style.display='none'" class="close"
            title="Close PopUp">&times;</span>
          <img src="../login/2.png" alt="Avatar" class="avatar">
          <h1 style="text-align:center">Nuevo Administrador</h1>
        </div>
        <div class="container">
          <textarea style="display:none;" name='Id'><?php echo $row['IdU']; ?></textarea>
          <input type="text" placeholder="Nombre" name="name" required>
          <input type="text" placeholder="Apellido Paterno" name="ApPa" required>
          <input type="text" placeholder="Apellido Materno" name="ApMa" required>
          <input type="email" placeholder="Correo" name="email" required >
          <input type="password" placeholder="Introdusca una contraseña" name="password" required>

          <button class="btn btn-lg btn-light btn-block" type="submit">Guardar</button>
        </div>
      </form>
    </div>
    <!---->
    </p>
    <br>
  </div>
    <div class="table-responsive">
      <table class="table table-striped">
        <tbody>
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Apellido Paterno</th>
              <th>Apellido Materno</th>
              <th>Correo</th>
            </tr>
          </thead>
          <?php foreach ($stmt as $row) {?>
          <tr>
            <?php //Echo $row['IdU']?>
            <td><?php Echo $row['Nombre']?></td>
            <td><?php Echo $row['ApPa']?></td>
            <td><?php Echo $row['ApMa']?></td>
            <td><?php Echo $row['Correo']?></td>
            <td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <!--fin formulario-->   
    <!--Tabla de Cliente-->
    <h2 id='TaPla'>Clientes</h2>
    <div class="table-responsive">
      <table class="table table-striped">
        <tbody>
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Correo</th>
              <th>Direccion</th>
            </tr>
          </thead>
          <?php foreach ($client as $row) {?>
          <tr>
            <td><?php Echo $row['Nombre']?> <?php Echo $row['ApPa']?> <?php Echo $row['ApMa']?></td>
            <td><?php Echo $row['Correo']?></td>
            <td>
        <p>Fraccionamiento/Colonia: <?php echo $row['Fracc']; ?></p>
        <p>Calle:<?php echo $row['Calle']; ?></p>
        <p>SMza: <?php echo $row['SMza']; ?> Mza: <?php echo $row['Mza']; ?> CP: <?php echo $row['CP']; ?></p>
        <p>Ciudad: <?php echo $row['Ciudad']; ?></p></td>
            
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <!--fin formulario-->      
  </main>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
    integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
    integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
    crossorigin="anonymous"></script>
  <script src="../JavaScrip/bootstrap.min.js"></script>
  <script src="../login/script.js" type="text"></script>
  <!--Activar - desactivar class active de un nav-item -->
  <script>
    $('li a').click(function (e) {
      //e.preventDefault();
      var $this = $(this);
      $this.closest('ul').find('li.active,a.active').removeClass('active');
      $this.addClass('active');
      $this.parent().addClass('active');

    });</script>
</body>

</html>