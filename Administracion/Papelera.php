<?php
require_once '../libs/DataBase.php';
$pdo = DataBase::getInstance();

$stmt = $pdo->query("SELECT * FROM Papelera_Plantas")->fetchAll();

$produ = $pdo->query("SELECT * FROM Papelera_Produc")->fetchAll(); 

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../img/icons/logo.ico">

  <title>Papelera</title>

  <!-- Bootstrap core CSS -->
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


  <!-- Custom styles for this template -->
  <link href="../css/dashboard.css" rel="stylesheet">
</head>

<body>

  <nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item ">
          <a class="nav-link" href="admin.php">Dashboard <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="cuentas.php">Cuentas</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="Papelera.php">Papelera</a>
        </li>
      </ul>
      <form class="form-inline my-2 my-lg-0" action="../libs/logout.php">
        <button class="btn btn-outline-light my-2 my-sm-0"  type="submit">Cerrar Sesion</button>
      </form>
    </div>
  </nav>
  <!--Columna-NAV-->
  <div class="container-fluid">
    <div class="row">
      <nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
        <ul class="nav nav-pills flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="#Inicio">Inicio <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#TaPla">Plantas <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link " href="#TaPro">Productos <span class="sr-only">(current)</span></a>
          </li>
      </nav>
    </div>
  </div>
  <!--Fin Columna-NAV-->
  <main class="col-sm-9 offset-sm-3 col-md-10 offset-md-2 pt-3" id='Inicio'>
    <h1>Papelera</h1>

    <!--Tabla de Productos/Plantas-->
    <h2 id='TaPla'>Plantas
              <form action="../libs/restaurar.php?de=5" method="POST" enctype="multipart/form-data">
                  <button type="submit" class="btn btn-outline-danger btn-lg ">Eliminar Todo</button>
              </form></h2>
    <div class="table-responsive">
      <table class="table table-striped">
        <tbody>
          <thead>
            <tr>
              <th>ID</th>
              <th>Nombre</th>
              <th>Tipo</th>
              <th>Cantidad</th>
              <th>Precio</th>
              <th>Descripcion</th>
              <th>Imagen</th>
              <th>Restaurar</th>
            </tr>
          </thead>
          <?php foreach ($stmt as $row) {?>
          <tr>
            <td><?php Echo $row['Id']?></td>
            <td><?php Echo $row['Nombre']?></td>
            <td><?php Echo $row['Tipo']?></td>
            <td><?php Echo $row['Cantidad']?></td>
            <td><?php Echo $row['Precio']?></td>
            <td><?php Echo $row['Descripcion']?></td>
            <td><img src="../img/plantas/<?php Echo $row['Imagen']?>" height="150" width="175" class="card-img-top"
                alt="..."></td>
            <td>
              <form action="../libs/restaurar.php?de=1&Id=<?php Echo $row['Id']?>&Nombre=<?php Echo $row['Nombre']?>&Tipo=<?php Echo $row['Tipo']?>&Cantidad=<?php Echo $row['Cantidad']?>&Precio=<?php Echo $row['Precio']?>&Descripcion=<?php Echo $row['Descripcion']?>&Imagen=<?php Echo $row['Imagen']?>" method="POST" enctype="multipart/form-data">
                  <button type="submit" class="btn btn-outline-info btn-lg btn-block">Restaurar</button>
              </form>
              <form action="../libs/restaurar.php?de=3&Id=<?php Echo $row['Id']?>" method="POST" enctype="multipart/form-data">
                  <button type="submit" class="btn btn-outline-danger btn-lg btn-block">Eliminar</button>
              </form>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <!--fin formulario-->
    <!--Tabla de Productos/Plantas-->
     <h2 id='TaPro'>Productos
              <form action="../libs/restaurar.php?de=6" method="POST" enctype="multipart/form-data">
                  <button type="submit" class="btn btn-outline-danger btn-lg ">Eliminar Todo</button>
              </form></h2>
        <div class="table-responsive">
          <table class="table table-striped">
            <tbody>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Total Vendidos</th>
                  <th>Precio</th>
                  <th>Imagen</th>
                  <th>Restaurar</th>
                </tr>
              </thead>
              <?php foreach ($produ as $row){?>
              <tr>
                <td><?php Echo $row['Id']?></td>
                <td><?php Echo $row['Nombre']?></td>
                <td><?php Echo $row['Cantidad']?></td>
                <td><?php Echo $row['Total_Vendidos']?></td>
                <td>$<?php Echo $row['Precio']?></td>
                <td><img src="../img/Productos/<?php Echo $row['Imagen']?>" height="100" width="100" alt="..."></td>
                <td>
              <form action="../libs/restaurar.php?de=2&Id=<?php Echo $row['Id']?>&Imagen=<?php Echo $row['Imagen']?>&Nombre=<?php Echo $row['Nombre']?>&Cantidad=<?php Echo $row['Cantidad']?>&Total_Vendidos=<?php Echo $row['Total_Vendidos']?>&Precio=<?php Echo $row['Precio']?>" method="POST" enctype="multipart/form-data">
                <button type="submit" class="btn btn-outline-info btn-lg btn-block">Restaurar</button>
              </form>
              <form action="../libs/restaurar.php?de=4&Id=<?php Echo $row['Id']?>" method="POST" enctype="multipart/form-data">
                  <button type="submit" class="btn btn-outline-danger btn-lg btn-block">Eliminar</button>
              </form>
            </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!--fin tabla-->       
  </main>

  <!-- Bootstrap core JavaScript
    ================================================== -->
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
    integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
    integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
    crossorigin="anonymous"></script>
  <script src="../JavaScrip/bootstrap.min.js"></script>
  <!--Activar - desactivar class active de un nav-item -->
  <script>
    $('li a').click(function (e) {
      //e.preventDefault();
      var $this = $(this);
      $this.closest('ul').find('li.active,a.active').removeClass('active');
      $this.addClass('active');
      $this.parent().addClass('active');

    });</script>
</body>

</html>