<?php
require_once '../libs/DataBase.php';
$pdo = DataBase::getInstance();

$Id = $_POST['Id'];
$Caso = $_GET['de'];

switch($Caso){
  case 1:
    $Nombre = $_POST['Nombre'];
    $Cantidad = $_POST['Cantidad'];
    $Precio = $_POST['Precio'];
    $Tipo = $_POST['Tipo'];
    $Descrip = $_POST['Descrip'];    
    switch($Tipo){
      case "Arbol":
        $id_Cui = 1;
      break;
      case "Arbustos":
        $id_Cui = 2;
      break;
      case "Trepadoras":
        $id_Cui = 3;
      break;
      case "Cactus":
        $id_Cui = 4;
      break;
      case "Crasas":
        $id_Cui = 5;
      break;
      case "Herbaceas":
        $id_Cui = 6;
      break;
      case "Hortícolas":
        $id_Cui = 7;
      break;
      case "Palmeras":
        $id_Cui = 8;
      break;
    }
    /*Se verifica que los campos esten llenos o no para realizar diferentes updates */
    $sql = 'CALL UpdatePlanta(?,?,?,?,?,?,?)';
    $stmt = $pdo->prepare($sql);
    $OK = $stmt->execute(array($Id, $Nombre, $Tipo, $Cantidad, $Precio, $Descrip, $id_Cui));

    /*Se verifica si existe algun error */
    $error = $stmt->errorInfo();
    if (!$OK) {
      echo $error[2] . "xxd";
    }
    else {
      echo "<script>
        window.location= '../Administracion/admin.php';
        alert('Actualizado Correctamente');
        </script>";
	    //header("refresh: 2;searchEmployee-02.php?id=".$_GET['id']);
    }
  break;
  case 2:
    $Nombre = $_POST['Nombre'];
    $Cantidad = $_POST['Cantidad'];
    $Precio = $_POST['Precio'];
    $sql = 'CALL UpdateProduct(?,?,?,?)';
    $stmt = $pdo->prepare($sql);
    $OK = $stmt->execute(array($Id, $Nombre, $Cantidad, $Precio));

    /*Se verifica si existe algun error */
    $error = $stmt->errorInfo();
    if (!$OK) {
      echo $error[2] . "xxd";
    }
    else {
	    echo "<script>
      window.location= '../Administracion/admin.php';
      alert('Actualizado Correctamente');
      </script>";
	    //header("refresh: 2;searchEmployee-02.php?id=".$_GET['id']);
    }
  break;
  case 3:
  $Name= $_POST['name'];
  $ApPa= $_POST['ApPa'];
  $ApMa= $_POST['ApMa'];
  $Tel= $_POST['Tel'];
    $sql = 'CALL Actua_Usa(?,?,?,?,?)';
    $stmt = $pdo->prepare($sql);
    $OK = $stmt->execute(array($Name, $ApPa, $ApMa, $Tel, $Id));

    /*Se verifica si existe algun error */
    $error = $stmt->errorInfo();
    if (!$OK) {
      echo $error[2] . "xxd";
    }
    else {
	    echo "<script>
      window.location= '../Profile/profile.php';
      alert('Actualizado Correctamente');
      </script>";
	    //header("refresh: 2;searchEmployee-02.php?id=".$_GET['id']);
    }
  break;
  case 4:
  $Password = $_POST['password'];
  $Password_new = $_POST['password_new'];

  $stmt2 = $pdo->query ("SELECT * FROM Contra_Sesion where Id='$Id'");
  $row = $stmt2->fetch();
  $error2 = $stmt2->errorInfo();
  $hash = $row['Contraseña'];
  
  if(password_verify($Password, $hash)){
    $Contra_new = password_hash($Password_new, PASSWORD_DEFAULT);
    $sql = 'CALL Actua_Contra(?,?)';
    $stmt = $pdo->prepare($sql);
    $OK = $stmt->execute(array($Contra_new, $Id));
  
    /*Se verifica si existe algun error */
    $error = $stmt->errorInfo();
    if (!$OK) {
      echo $error[2] . "xxd";
    }
    else {
      echo "<script>
      window.location= '../Profile/profile.php';
      alert('Actualizado Correctamente');
      </script>";
      //header("refresh: 2;searchEmployee-02.php?id=".$_GET['id']);
    }
  }
  else{
    echo $row['Contraseña'] . $row['Id'] . $Id;
    echo $Password . "<br>" . $hash . "<br>" . $row['Contraseña'];
    echo $error2[2] . "xxd";
  }
  break;
  
  case 5:
  $Fracc= $_POST['Fracc'];
  $Calle= $_POST['Calle'];
  $Sm= $_POST['SMza'];
  $Mza= $_POST['Mza'];
  $Lt= $_POST['Lt'];
  $Cp= $_POST['CP'];
  $Ciudad= $_POST['Ciudad'];
    $sql = "CALL Actua_Dir(?,?,?,?,?,?,?,?)";
    $stmt = $pdo->prepare($sql);
    $OK = $stmt->execute(array($Fracc, $Calle, $Sm, $Mza, $Lt, $Cp, $Ciudad, $Id));

    /*Se verifica si existe algun error */
    $error = $stmt->errorInfo();
    if (!$OK) {
      echo $error[2] . "xxd";
    }
    else {
	    echo "<script>
      window.location= '../Profile/profile.php';
      alert('Actualizado Correctamente');
      </script>";
	    //header("refresh: 2;searchEmployee-02.php?id=".$_GET['id']);
    }
  break;
  case 6:
  $Fracc= $_POST['Fracc'];
  $Calle= $_POST['Calle'];
  $Sm= $_POST['SMza'];
  $Mza= $_POST['Mza'];
  $Lt= $_POST['Lt'];
  $Cp= $_POST['CP'];
  $Ciudad= $_POST['Ciudad'];
    echo $Id .  "<br>";
    
  
  $insertDir = "CALL NewDir(?,?,?,?,?,?,?)";
  $ResultDir = $pdo->prepare($insertDir);
  $ExecDir = $ResultDir->execute(array($Fracc, $Calle, $Sm, $Mza, $Lt, $Cp, $Ciudad));

  $Id_Dir = $pdo->query("SELECT * FROM Ultimo_Dir")->fetch();
  $ID_DirN= $Id_Dir['Id'];
    $sql = "CALL Actua_Dir_U(?,?)";
    $stmt = $pdo->prepare($sql);
    $OK = $stmt->execute(array($ID_DirN, $Id));

    /*Se verifica si existe algun error */
    $error = $stmt->errorInfo();
    if (!$OK) {
      echo $error[2] . "xxd";
    }
    else {
	    echo "<script>
      window.location= '../Profile/profile.php';
      alert('Actualizado Correctamente');
      </script>";
	    //header("refresh: 2;searchEmployee-02.php?id=".$_GET['id']);
    }
  break;
}
