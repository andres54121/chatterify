/* Destroy current user session */

<?php
include '../Carrito/cart.php';
$cart = new Cart;

$cart->destroy();
session_start();
if($_SESSION['Permisos']=3){
    header("Location: ../login.html");
}
else{
    session_destroy();
    session_start();
    $_SESSION['Permisos']=3;
    header("Location: ../index.php");
}


?>