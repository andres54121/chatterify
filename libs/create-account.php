<?php
require_once '../libs/DataBase.php';
$pdo = DataBase::getInstance();
//Datos para guardar en Sesiones
$Email = $_POST['email'];
$Password = $_POST['password'];
$Lvl_Per = $_GET['lvl'];

//Datos para guardar en Usuario
$Name= $_POST['name'];
$ApPa= $_POST['ApPa'];
$ApMa= $_POST['ApMa'];


//Se checa que el correo del nuevo usuario no este registrado en la base de datos
$stmt = $pdo-> query("SELECT  COUNT(*) as total FROM sesiones WHERE Correo = '$Email' ");
$checkEmail = $stmt-> fetch();

if ($checkEmail['total'] == 1) {
        echo "<script>
        window.location= '../login.html';
        alert('Este correo ya se registro previamente');
        </script>";
}
//Si no esta entonces se procedera a almacenar el nuevo usuario en la base de datos
else{
  //Se encripta la contraseña
  $passHash = password_hash($Password, PASSWORD_DEFAULT);
  $insertSes = "CALL NewSession(?,?,?)";
  $ResultSes = $pdo->prepare($insertSes);
  $ExecSes = $ResultSes->execute(array($Email, $passHash, $Lvl_Per));
  
  $Id_ses = $pdo->query("SELECT * FROM Ultimo_Sesion")->fetch();
  $Id_Dir = 0;
  $img="default.jpg";

  $insertUsua = "CALL NewUsua(?,?,?,?,?,?)";
  $ResultUsua = $pdo->prepare($insertUsua);
  $ExecUsua = $ResultUsua->execute(array($Name, $ApPa, $ApMa, $img, $Id_Dir, $Id_ses['Id']));
  if($Lvl_Per==1){
    echo "<script>
    window.location= '../Administracion/cuentas.php.html';
    alert('Agregado Correctamente');
    </script>";

  }else{
    echo "<script>
    window.location= '../login.html';
    alert('Agregado Correctamente');
    </script>";
  }
  
}