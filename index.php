<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    
  <!--Otro CSS
  <link rel="stylesheet" href="./css/Supreme.css">
  -->
  <link rel="stylesheet" href="./css/chater.css">
  
  <link rel="icon" href="./img/icons/logo.ico">
    <title>Chaterify</title>
</head>

<body>
        <!--Barra de navegacion-->
        <?php include('Nav.php');?>
        <!--Fin Barra-->
    <!--Fin Barra-->
    <!--Carrusel de imagenes-->
    <br><br>
    <div class="bd-example">
        <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
                <li data-target="#carouselExampleCaptions" data-slide-to="3"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active parallax" id="banner4">
                </div>
                <div class="carousel-item  parallax" id="banner1">
                </div>
                <div class="carousel-item parallax" id="banner2">
                </div>
                <div class="carousel-item parallax" id="banner3">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <!---->
    <!--Cartas menu-->
    <div class="container marketing">
        <div id="cart" class="div-cent row">
            <div class="card" style="width: 18rem;">
                <img src="img/plantas/9556710965_efe7de9eb8_b.jpg " height="175" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">¿Quienes somos?</h5><br>
                    <p class="card-text">
                        Te contaremos nuestra vida crack.
                    </p><br>
                    <a href="info.php" class="btn btn-primary">Ver más...</a>
                </div>
            </div>
            <div class="card " style="width: 18rem; ">
                <img src="img/plantas/memoria-plantas_EDIIMA20180606_0066_19.jpg" height="175" class="card-img-top"
                    alt="...">
                <div class="card-body ">
                    <h5 class="card-title">Nuestras Plantas</h5>
                    <p class="card-text">
                        Conoce nuestra amplia gama de plantas que cultivamos con mucho amor.
                    </p>
                    <a href="plantas.php" class="btn btn-primary">Ver más...</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <img src="img/plantas/plantas-vivaces-flor-tardia.jpg" height="175" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Nuestros Productos</h5>
                    <p class="card-text">
                        Nuestros productos de alta calidad que le podran ayudar a cuidar mejor sus plantas.
                    </p>
                    <a href="producto.php" class="btn btn-primary">Ver más...</a>
                </div>
            </div>
        </div>
    </div>
    <!--Fin Cartas-->
    <!-- FOOTER -->
    <object type="text/x-scriptlet" width=100% height="270" scrolling="no" data="Footer.html">
    </object>
    <!--Fin Footer-->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    
</body>

</html>