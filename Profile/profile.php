<?php
require_once '../libs/DataBase.php';
$pdo = DataBase::getInstance();

session_start();
$ID=$_SESSION['ID_ses'];

$stmt = $pdo->query ("SELECT * FROM Info_Usua where id='$ID'");
$row = $stmt->fetch();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../img/icons/logo.ico">

  <title>Perfil</title>

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <link href="../css/profile.css" rel="stylesheet">
</head>
<!------ Include the above in your HEAD tag ---------->

<body>
  <div class="container emp-profile">
    <div class="row">
      <div class="col-md-4">
        <div class="profile-img">
          <img src="../img/perfil/<?php echo $row['imagen']; ?>" alt="" width="280" height="180" ; />
          <div class="file btn btn-lg btn-primary">
            Change Photo
            <input type="file" name="file" />
          </div>
          <div class="profile-work">
            <p></p>
            <p></p>
            <button type="button" class="btn btn-outline-primary btn-lg btn-block" onclick="location.href='../index.php'">Seguir Comprando</button>
            <p></p>
            <button type="button" class="btn btn-outline-primary btn-lg btn-block" onclick="location.href='compras.php'">Mis Compras</button>
            <p></p>
            <form class="form-inline my-2 my-lg-0" action="../libs/logout.php">
              <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Cerrar Sesión</button>
            </form>
            <p></p>


            


          </div>
        </div>

      </div>
      <div class="col-md-6">
        <div class="shipAddr">
          <h2>Datos Personales</h2>
          <h4>Nombre:</h4>
          <p><?php echo $row['Nombre']; ?> <?php echo $row['ApPa']; ?> <?php echo $row['ApMa']; ?></p>

          <h4>Correo Electronico:</h4>
          <p><?php echo $row['Correo']; ?></p>
          <h4>Telefono: </h4><?php echo $row['Tel']; ?>
          <h2>Direccion:</h4>
            <p><b>Fraccionamiento/Colonia: </b><?php echo $row['Fracc']; ?></p>
            <p><b>Calle: </b><?php echo $row['Calle']; ?></p>
            <p><b>SMza: </b> <?php echo $row['SMza']; ?> <b>Mza: </b> <?php echo $row['Mza']; ?> <b>CP:</b>
              <?php echo $row['CP']; ?>
            </p>
            <p><b>Ciudad: </b> <?php echo $row['Ciudad']; ?></p>
        </div>
      </div>
      <div class="col-md-2">
      </div>
    </div>
    <div class="row">
      <div class="col-md-4">

      </div>
      <div class="inner cover row">
    <!--Boton Editar Perfil-->
    <button class="btn btn-outline-primary btn-lg "
      onclick="document.getElementById('modal-wrapper').style.display='block'">Editar Perfil</button>
    <div id="modal-wrapper" class="modal">
      <form class="modal-content animate" action="../libs/update.php?de=3" method="POST">
        <div class="imgcontainer">
          <span onclick="document.getElementById('modal-wrapper').style.display='none'" class="close"
            title="Close PopUp">&times;</span>
          <img src="../img/perfil/<?php echo $row['imagen']; ?>" alt="Avatar" class="avatar">
          <h1 style="text-align:center">Editar Perfil</h1>
        </div>
        <div class="container">
          <textarea style="display:none;" name='Id'><?php echo $row['IdU']; ?></textarea>
          <input type="text" placeholder="Nombre" name="name" required>
          <input type="text" placeholder="Apellido Paterno" name="ApPa" required>
          <input type="text" placeholder="Apellido Materno" name="ApMa" required>
          <input type="number" min="1" max="9999999999" placeholder="Telefono" name="Tel">

          <button class="btn btn-lg btn-light btn-block" type="submit">Guardar Cambios</button>
        </div>
      </form>
    </div>
    <!---->
    <!--Boton Agregar Direccion-->
    <button class="btn btn-outline-primary btn-lg "
      onclick="document.getElementById('modal-wrapper3').style.display='block'">Cambiar contraseña</button>
    <div id="modal-wrapper3" class="modal">
      <form class="modal-content animate" action="../libs/update.php?de=4" method="POST">
        <div class="imgcontainer">
          <span onclick="document.getElementById('modal-wrapper3').style.display='none'" class="close"
            title="Close PopUp">&times;</span>
          <img src="../login/1.png" alt="Avatar" class="avatar">
          <h1 style="text-align:center">Cambiar contraseña</h1>
        </div>
        <div class="container">
          <h3 style="text-align:center">Contraseña Actual</h3>
          <textarea style="display:none;" name='Id'><?php echo $row['ID']; ?></textarea>
          <input type="password" placeholder="" name="password" required>
          <h3 style="text-align:center">Contraseña Nueva</h3>
          <input type="password" placeholder="" name="password_new" required>
          <button class="btn btn-lg btn-light btn-block" type="submit">Guardar</button>
        </div>
      </form>
    </div>
    <!---->

    <?php if($row['Id_Dir']==0){ ?>
    <!--Boton Agregar direccion-->
    <button class="btn btn-outline-primary btn-lg "
      onclick="document.getElementById('modal-wrapper4').style.display='block'">Agregar Direccion</button>
    <div id="modal-wrapper4" class="modal">
      <form class="modal-content animate" action="../libs/update.php?de=6" method="POST" autocomplete="off">
        <div class="imgcontainer">
          <span onclick="document.getElementById('modal-wrapper4').style.display='none'" class="close"
            title="Close PopUp">&times;</span>
          <img src="../login/2.png" alt="Avatar" class="avatar">
          <h1 style="text-align:center">Agregar Direccion</h1>
        </div>
        <div class="container">

          <textarea style="display:none;" name='Id'><?php echo $row['IdU']; ?></textarea>
          <input type="text" placeholder="Fraccionamiento o Colonia" name="Fracc" required>
          <input type="text" placeholder="Calles" name="Calle" required>
          <input type="number" min="1" pattern="^[0-9]+" placeholder="SMza" name="SMza" required>
          <input type="number" placeholder="Mza" name="Mza">
          <input type="number" placeholder="Lote" name="Lt">
          <input type="number" placeholder="Codigo Postal" name="CP" required>
          <input type="text" placeholder="Ciudad" name="Ciudad">

          <button class="btn btn-lg btn-light btn-block" id="crear" type="submit">Guardar</button>
        </div>
      </form>
    </div>
    <!---->
    <?php }else{ ?>

    <!--Boton Editar direccion-->
    <button class="btn btn-outline-primary btn-lg"
      onclick="document.getElementById('modal-wrapper2').style.display='block'">Editar Direccion</button>
    <div id="modal-wrapper2" class="modal">
      <form class="modal-content animate" action="../libs/update.php?de=5" method="POST" autocomplete="off">
        <div class="imgcontainer">
          <span onclick="document.getElementById('modal-wrapper2').style.display='none'" class="close"
            title="Close PopUp">&times;</span>
          <img src="../login/2.png" alt="Avatar" class="avatar">
          <h1 style="text-align:center">Editar Direccion</h1>
        </div>
        <div class="container">
          <textarea style="display:none;" name='Id'><?php echo $row['Id_Dir']; ?></textarea>
          <input type="text" placeholder="Fraccionamiento o Colonia" name="Fracc" required>
          <input type="text" placeholder="Calles" name="Calle" required>
          <input type="number" min="1" pattern="^[0-9]+" placeholder="SMza" name="SMza" required>
          <input type="number" placeholder="Mza" name="Mza">
          <input type="number" placeholder="Lote" name="Lt">
          <input type="number" placeholder="Codigo Postal" name="CP" required>
          <input type="text" placeholder="Ciudad" name="Ciudad">

          <button class="btn btn-lg btn-light btn-block" id="crear" type="submit">Guardar Cambios</button>
        </div>
      </form>
    </div>
    <!--****************************************************-->
    <?php } ?>
    </p>
    <br>
  </div>

    </div>
  </div>


<!--Flotante-->
  <div class="inner cover"><br>
    <!--Boton Editar Perfil-->
    <button class="btn btn-outline-primary btn-lg "
      onclick="document.getElementById('modal-wrapper').style.display='block'">Editar Perfil</button>
    <div id="modal-wrapper" class="modal">
      <form class="modal-content animate" action="../libs/update.php?de=3" method="POST">
        <div class="imgcontainer">
          <span onclick="document.getElementById('modal-wrapper').style.display='none'" class="close"
            title="Close PopUp">&times;</span>
          <img src="../img/perfil/<?php echo $row['imagen']; ?>" alt="Avatar" class="avatar">
          <h1 style="text-align:center">Editar Perfil</h1>
        </div>
        <div class="container">
          <?php ?>
        </div>
      </form>
    </div>
    <!---->
    </p>
    <br>
  </div>
<!--Fin flotante-->

  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
    integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
    integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
    crossorigin="anonymous"></script>
  <script src="./JavaScrip/bootstrap.min.js"></script>
  <script src="./login/script.js" type="text"></script>
</body>

</html>