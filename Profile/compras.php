<?php
require_once '../libs/DataBase.php';
$pdo = DataBase::getInstance();

session_start();
$ID=$_SESSION['ID_ses'];

$stmt3 = $pdo->query ("SELECT * FROM Info_Usua where id='$ID'");
$row = $stmt3->fetch();
$stmt2 = $pdo->query("SELECT * FROM Plant_Co where Id_u='$ID'")->fetchAll();
$stmt = $pdo->query("SELECT * FROM Prod_Co where Id_u='$ID'")->fetchAll();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="../img/icons/logo.ico">

  <title>Compras</title>

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

  <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <link href="../css/profile.css" rel="stylesheet">
</head>
<!------ Include the above in your HEAD tag ---------->

<body>
  <div class="container emp-profile">
    <div class="row">
      <div class="col-md-4">
        <div class="profile-img">
          <img src="../img/perfil/<?php echo $row['imagen']; ?>" alt="" width="280" height="180" ; />
          <div class="file btn btn-lg btn-primary">
            Change Photo
            <input type="file" name="file" />
          </div>
          <div class="profile-work">
            <p></p>
            <p></p>
            <button type="button" class="btn btn-outline-primary btn-lg btn-block" onclick="location.href='profile.php'">Mi Perfil</button>
            <p></p>
            <button type="button" class="btn btn-outline-primary btn-lg btn-block" onclick="location.href='../index.php'">Seguir Comprando</button>
            <p></p>
            <form class="form-inline my-2 my-lg-0" action="../libs/logout.php">
              <button type="submit" class="btn btn-outline-primary btn-lg btn-block">Cerrar Sesión</button>
            </form>
            <p></p>

          </div>
        </div>

      </div>
      <div class="col-md-8">
        <div class="shipAddr">
            <!--Tabla de Plantas-->
        <h2 id='TaPla'>Plantas</h2>
        <div class="table-responsive">
          <table class="table table-striped">
            <tbody>
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Precio Unitario</th>                  
                  <th>Total</th>
                  <th>Fecha de Compra</th>
                  <th>Estatus</th>
                </tr>
              </thead>
              <?php foreach ($stmt2 as $row) {?>
              <tr>
                <td><?php Echo $row['Nombre']?></td>
                <td><?php Echo $row['Cantidad']?></td>
                <td>$<?php Echo $row['Precio']?></td>
                <td>$<?php Echo $row['total']?></td>                
                <td>Hoy xd<?php //Echo $row['Total']?></td>                
                <td>entregado xd<?php //Echo $row['Total']?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!--fin-->
        <!--Tabla de Productos-->
        <h2 id='TaPla'>Productos</h2>
        <div class="table-responsive">
          <table class="table table-striped">
            <tbody>
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Cantidad</th>
                  <th>Precio Unitario</th>                  
                  <th>Total</th>
                  <th>Fecha de Compra</th>
                  <th>Estatus</th>
                </tr>
              </thead>
              <?php foreach ($stmt as $row) {?>
              <tr>
                <td><?php Echo $row['Nombre']?></td>
                <td><?php Echo $row['Cantidad']?></td>
                <td>$<?php Echo $row['Precio']?></td>
                <td>$<?php Echo $row['total']?></td>                
                <td>Hoy xd<?php //Echo $row['Total']?></td>                
                <td>entregado xd<?php //Echo $row['Total']?></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!--fin-->
        
    <div class="row">
      <div class="col-md-4">

      </div>


    </div>
  </div>



  <!-- Placed at the end of the document so the pages load faster -->
  <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
    integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
    crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
    integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
    crossorigin="anonymous"></script>
  <script src="./JavaScrip/bootstrap.min.js"></script>
  <script src="./login/script.js" type="text"></script>
</body>

</html>