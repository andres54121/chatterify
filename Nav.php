<?php

?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
    <!--Barra de navegacion-->
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Lotusbloem</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="index.php">Inicio <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="plantas.php">Plantas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="producto.php">Productos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="info.php">Acerca de...</a>
                    </li>
                    <li class="nav-item">
                    <?php
                    session_start();
                    if ($_SESSION["Permisos"] == 3){
                        echo '<a class="nav-link disabled" href="admin.php" tabindex="-1" aria-disabled="true">Administrar</a>';

                    }else{
                        echo '<a class="nav-link " href="Profile/profile.php">Perfil</a>';
                    }
                    ?>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0" action="./libs/logout.php">
                    <?php
                    if ($_SESSION["Permisos"] == 3){ ?>
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Iniciar Sesion</button><?php
                    }else{?>
                        <a class="btn btn-outline-success my-2 my-sm-0"href="./carrito/viewCart.php" class="cart-link" title="View Cart">Carrito</a>
                        <button class="btn btn-outline-success my-2 my-sm-0"  type="submit">Cerrar Sesion</button><?php
                    }
                    ?>
                </form>
            </div>
        </nav>
    </header>
    <!--Fin Barra-->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
    
</body>

</html>