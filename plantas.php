<?php
require_once './libs/DataBase.php';
$pdo = DataBase::getInstance();

$stmt = $pdo->prepare("SELECT * FROM plantas_info2"); 
$stmt->execute();

// set the resulting array to associative
$stmt->setFetchMode(PDO::FETCH_ASSOC);

    /* creamos un while para optener fila a fila los refultados devueltos mediante fetch() 
    while ($row = $stmt->fetch()) {
        echo $row['Nombre'] . " - " . $row['Imagen'] ."<br>";
        echo "xdxdxd";
    }*/
?>
<!doctype html>
<html lang="en">

<head>
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link rel="stylesheet" href="./css/sty.css">

  <!--Otro CSS
  <link rel="stylesheet" href="./css/Supreme.css">
  -->
  <link rel="stylesheet" href="./css/chater.css">
  
  <link rel="icon" href="./img/icons/logo.ico">

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Plantas</title>
</head>

<body>
  <!--Barra de navegacion-->
  <?php include('Nav.php');?>
  <!--Fin Barra-->

  <!--Carrusel de fotos-->
  <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <div class="carousel-item active parallax" id="plant1">
      </div>
      <div class="carousel-item parallax" id="plant2">
      </div>
      <div class="carousel-item parallax" id="plant3">
        <!--<img src="img/Banners/wood-1350175_1920.jpg" height="800" class="d-block w-100">-->
      </div>
    </div>
  </div>
  <!--Fin carrusel de fotos-->

  <!--Cartas Plantas--><br>
  <div class="container marketing">
    <div id="cart" class="div-cent row">
      <!--Se manda a llamar mediante un while todas las plantas de la BD -->
      <?php while ($row = $stmt->fetch()) {
        if($row['Cantidad']!=0){
        ?>


      <div class="card" style="width: 18rem; ">
        <div class="card-image waves-effect waves-block waves-light">
          <img class="activator" src="img/plantas/<?php Echo $row['Imagen']?>" height="175" class="card-img-top">
        </div>
        <div class="card-content" text-align="center">
          <span class="card-title activator grey-text text-darken-4">
            <h3><?php echo $row['Nombre'] ?></h3>
            
          <p class="card-text"><h5>Precio: $<?php echo $row['Precio'] ?></h4></p>
          <a class="btn btn-success" href="./Carrito/cartAction.php?action=addToCart&id=<?php echo $row["Id"]; ?>&x=2&ca=<?php echo $row["Cantidad"]; ?>">Add to cart</a>

          </span>
        </div>
        <div class="card-reveal">
          <span class="card-title grey-text text-darken-4">
            <h4>Decripcion<i class="material-icons right">close</i></h4>
          </span>
          <p><?php echo $row['Descripcion'] ?></p>
          <p><a href="plantas.php" class="btn btn-primary">Cuidados</a></p>
        </div>
      </div>
      <?php } } ?>
    </div>
  </div>
  <!--Fin Cartas Plantas-->

  <!-- FOOTER -->
  <object type="text/x-scriptlet" width=100% height="270" scrolling="no" data="Footer.html">
  </object>
  <!--Fin Footer-->
  <!--JavaScript at end of body for optimized loading-->
  <script type="text/javascript" src="./Materia/js/materialize.min.js"></script>

  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script>
</body>

</html>